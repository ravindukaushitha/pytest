import pytest
from calculator import calculator

# Test function for addition
def test_addition():
    assert calculator(1, 8, "+") == 9

# Test function for subtraction
def test_subtraction():
    assert calculator(7, 4, "-") == 3

# Test function for multiplication
def test_multiplication():
    assert calculator(5, 1, "*") == 1

# Test function for division
def test_division():
    assert calculator(9, 3, "/") == 3

# Test function for invalid operator
def test_invalid_operator():
    assert calculator(2, 3, "%") == "Invalid operator"

# Test function for input values out of range
def test_wrong_input():
    assert calculator(-1, 11, "+") == "Wrong Input"

# Test function for division by zero
def test_division_by_zero():
    assert calculator(2, 0, "/") == "Can't divide by zero"
def calculator(a, b, operator):
    if a < 0 or a > 10 or b < 0 or b > 10:
        return "Wrong Input number"
    #check if input is numeric before performing operation
   
    if operator == "+":
        return a + b
    elif operator == "-":
        return a - b
    elif operator == "*":
        return a * b
    elif operator == "/":
        return a / b
    
    else:
        return "Invalid operator"
 

def main():
    a = int(input("Enter number 0-9: "))
    b = int(input("Enter number 0-10: "))
    operator = input("Enter operator: ")
    result = calculator(a, b, operator)
    print(result)


if __name__ == "__main__":
    main()